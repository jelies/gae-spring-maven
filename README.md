Google App Engine + Spring + Eclipse + Maven
============================================
Base project with Google App Engine (1.7.5), Spring (3.2.1) and Maven. Having problems launching app from eclipse, seems like spring-web-3.2.1 has some problems with Google Eclipse Plugin. Check StackOverflow's question here: http://stackoverflow.com/q/15174774/787375

Easy mode: Maven (completed!)
-----------------------------
1. run: `mvn clean package gae:run`
2. At *target/gae-spring-maven/WEB-INF/lib* you can find all POM's dependencies.

Hard Mode: Eclipse + Maven (still trying...)
--------------------------------------------
1. Add project to eclipse (import from existing maven project...)
2. Run with gae (eclipse generates war into *target/gae-spring-maven-1.0.0-SNAPSHOT*). BOOM, `java.lang.ClassNotFoundException: org.springframework.web.context.ContextLoaderListener`
3. Check out *target/gae-spring-maven-1.0.0-SNAPSHOT/WEB-INF/lib* folder. All POM's dependencies appear there **but spring-web**.

UPDATE
------
Using spring version `3.1.4.RELEASE` instead of `3.2.1.RELEASE`, works from maven console and from eclipse. **Yes, the spring-web is being published to *WEB-INF/lib* with Eclipse using spring `3.1.4.RELEASE`.** 
To try with `3.1.4.RELEASE`, uncomment `spring.version` property from `pom.xml` and uncomment `CGLIB`'s dependency.



Please help! :) 
