package com.jelies.gaespring.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "main")
public class MainController {

	private final Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName());

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView helloWorld() {
		log.debug("Hello Spring!");
		return null;
	}

}
