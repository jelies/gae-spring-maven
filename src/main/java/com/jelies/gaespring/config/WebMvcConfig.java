package com.jelies.gaespring.config;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.jelies.gaespring.controller")
public class WebMvcConfig extends WebMvcConfigurerAdapter {

	private final Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName());

	@PostConstruct
	public void init() {
		log.debug("WebMvcConfig initialized.");
	}

}
