package com.jelies.gaespring.config;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = { "com.jelies.gaespring" })
public class RootConfig {

	private final Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName());

	@PostConstruct
	public void init() {
		log.debug("RootConfig initialized.");
	}


}
